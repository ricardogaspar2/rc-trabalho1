import java.io.*;
import java.net.*;
import java.util.*;

public class GasparTftp {

	// static final int SERVERPORT = 9069;
	// static final String LOCALSERVER = "localhost";
	static final int BLOCKSIZE = 512;
	static DatagramSocket socket;
	static final String MODE = "ascii";
	static final int DATABYTES = 4;

	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.err.printf("usage: java MyTftp host port [-s blksize] \n");
			System.exit(0);
		}
		// Get server IP address and create an UDP socket
		String server = args[0];
		int serverPort = Integer.parseInt(args[1]);
		int blockSize = -1;
		// verifying optional blocksize
		if (args.length > 2) {
			if (args[2].equalsIgnoreCase("-s"))
				blockSize = Integer.parseInt(args[3]);
		}
		InetAddress serverAddress = InetAddress.getByName(server);
		socket = new DatagramSocket();

		// Read a message from the input console
		Scanner in = new Scanner(System.in);
		System.out
				.printf("MyTftp - please type the filename of the file you want to transfer: ");
		String requestedFilename = in.next();

		// Prepare a datagram to send to the server in a compact way
		// send it, wait for the reply, print it

		// Tftp frame for a READ tftp request
		byte[] tftpFrame = new byte[requestedFilename.length() + MODE.length()
				+ 4];
		DatagramPacket datagram = new DatagramPacket(tftpFrame,
				tftpFrame.length, serverAddress, serverPort);
		TftpPacket requestPacket = new TftpPacket(datagram.getData(),
				datagram.getLength());
		requestPacket.setOpcode(TftpPacket.RRQ);
		requestPacket.setFilename(requestedFilename, MODE);
		socket.send(datagram);

		// preparing to receive the reply message
		byte[] buffer = new byte[BLOCKSIZE];
		DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
		// receive msg from server
		socket.receive(reply);
		TftpPacket replyPkt = new TftpPacket(reply.getData(), reply.getLength());
		
		if (replyPkt.getOpcode() == TftpPacket.DATA) {
			System.out.println("DATA Reply");
			receiveBlocks(requestedFilename, reply,
					replyPkt);
		} else {
			System.out.println("ERROR Reply");
			System.out.printf("? Request %d ignored\n", replyPkt.getOpcode());
		}

		socket.close();
	} // main

	/**
	 * This method receives blocks, creates the file and
	 * 
	 * @param serverAddress
	 * @param serverPort
	 * @param filename
	 */
	private static void receiveBlocks(String filename, DatagramPacket reply,
			TftpPacket replypkt) {
		InetAddress serverAddress = reply.getAddress();
		int serverPort = reply.getPort();
		// prepare buffer for receiving
		try {

			FileOutputStream fout;
			if (serverAddress.isLoopbackAddress())
				fout = new FileOutputStream("clientFile_" + filename);
			else
				fout = new FileOutputStream(filename);

			byte[] fileBuffer = new byte[BLOCKSIZE];
			System.out.println("Lenght of reply: " + reply.getLength());
			boolean last = false;
			long totalBytes=0;
			long totalBlocks=0;
			long totalPackets=0;
			long totalACKs=0;
			
			do {
				fileBuffer = reply.getData();
				fout.write(fileBuffer, DATABYTES, reply.getLength() - DATABYTES);
				System.out.println("received "
						+ (reply.getLength() - DATABYTES) + " bytes");
				// send acknoledge
				DatagramPacket ackMsg = new DatagramPacket(new byte[DATABYTES],
						DATABYTES, serverAddress, serverPort);
				TftpPacket ackPkt = new TftpPacket(ackMsg.getData(),
						ackMsg.getLength());
				ackPkt.setOpcode(TftpPacket.ACK);
				ackPkt.setBlockCount(replypkt.getBlockCount());
				socket.send(ackMsg);

				if (reply.getLength() < BLOCKSIZE)
					last = true;
				else
					socket.receive(reply);

			} while (!last);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.printf("Can't copy file\n");
			System.exit(0);
		}

	}

}