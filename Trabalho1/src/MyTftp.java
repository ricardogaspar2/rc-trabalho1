import java.io.*;
import java.net.*;


//
//  @author Luis Silva, nr 34535, Turno P6
//  @author Ricardo Cruz nr 34951 Turno P6
//  @author Ricardo Gaspar nr 35277 Turno P6
// 
//  Docente: Henrique Joao Domingos
//  
//  Classe MyTftp
//  
//  
// 
// 
public class MyTftp {

	static final int BLOCKSIZE = 512;
	// numero de bytes correspondentes a frame Tftp DATA
	static final int DATABYTES = 4;
	static final String MODE = "octet";
	static DatagramSocket socket;

	public static void main(String[] args) throws IOException {

		if (args.length < 3) {
			System.out
					.printf("usage: java MyTftp host port filename[-s blksize]\n");
			System.exit(0);
		}

		// Dados de endereco e porto do servidor
		String server = args[0];
		int port = Integer.parseInt(args[1]);
		InetAddress serverAddress = null;
		try {
			serverAddress = InetAddress.getByName(server);
		} catch (UnknownHostException e) {
			System.out.println("Erro na convercao de DNS");
			e.printStackTrace();
			System.exit(0);
		}

		int blockSize = -1;
		// verifying optional blocksize
		if (args.length > 3) {
			if (args[3].equalsIgnoreCase("-s"))
				blockSize = Integer.parseInt(args[4]);
		}
		else
			blockSize = BLOCKSIZE;
		

		// Nome do ficheiro para o request
		String fileName = args[2];

		// Cria um socket para envio e recepcao de pacotes
		try {
			socket = new DatagramSocket();
		} catch (SocketException e) {
			System.out.println("Erro na criacao do socket");
			e.printStackTrace();
			System.exit(0);
		}

		// Chama o metodo para enviar o Request do ficheiro.
		sendRequest(fileName, serverAddress, port, blockSize);
		if(blockSize != BLOCKSIZE)
			blockSize = receiveBlockSizeAck();

		receiveBlock(fileName, blockSize);
		socket.close();

	}

	private static int receiveBlockSizeAck() throws IOException {
		DatagramPacket ackReply = new DatagramPacket(new byte[BLOCKSIZE + 4], BLOCKSIZE + 4);
		TftpPacket acktftpReply = new TftpPacket(ackReply.getData(), ackReply.getLength());
		int newBlockSize = 0;
		socket.receive(ackReply);
		if(acktftpReply.getOpcode() == 6){
			//System.out.println(new String(ackReply.getData(), 10, ackReply.getLength() - 10));
			newBlockSize = Integer.parseInt(new String(ackReply.getData(), 10, ackReply.getLength()- 10).trim());
			//System.out.println("Tamanho recebido no ack: " + newBlockSize);
			
			sendAck(0, ackReply.getAddress(), ackReply.getPort());
			
//			DatagramPacket ack = new DatagramPacket(new byte[BLOCKSIZE + 4], BLOCKSIZE + 4, ackReply.getAddress(), ackReply.getPort());
//			TftpPacket acktftp = new TftpPacket(ackReply.getData(), ackReply.getLength());
//			acktftp.setOpcode(TftpPacket.ACK);
//			acktftp.setBlockCount(0);
//			socket.send(ack);
		}else{
			System.out.println("ERROR ACK BlockSize");
			System.exit(0);
		}
		
		return newBlockSize;
		
	}

	private static void receiveBlock(String fileName, int blockSize) throws IOException {
		byte[] fileBuffer = new byte[blockSize + DATABYTES];

		DatagramPacket reply = new DatagramPacket(fileBuffer, fileBuffer.length);
		TftpPacket tftpReply = new TftpPacket(reply.getData(),
				reply.getLength());

		boolean isFinished = false;
		long packetsReceived = 0;
		long totalBytes = 0;
		long totalPacketsReceived = 0;
		long totalDuplicatePackets = 0;
		long totalACKs = 0;
		long startTime = System.currentTimeMillis();
		long sumACKnPacketTime = 0;
		long countACKnPacket = 0;
		long startACKTime = 0;
		long endACKTime = 0;

		try { // Tenta receber o primeiro pacote
			socket.receive(reply);
			//System.out.println(new String("TFPT REPLY: " + tftpReply.getOpcode()));
			if(tftpReply.getOpcode() != TftpPacket.DATA){
				System.out.println("ERROR Reply");
				System.out.printf("? Request %d ignored\n", tftpReply.getOpcode());
				System.exit(0);
			}
		} catch (IOException e) {
			System.out.println("Erro no socket receive");
			e.printStackTrace();
		}

		// Mudanca do endereco do servidor e do porto para o que vem na resposta
		// para conbinar com o da resposta.
		InetAddress serverAddress = reply.getAddress();
		int port = reply.getPort();

		FileOutputStream fout;
		// Quando o ficheiro estiver num servidor local o nome do ficheiro tem
		// um nome diferente
		if (serverAddress.isLoopbackAddress())
			fout = new FileOutputStream("clientFile_" + fileName);
		else
			fout = new FileOutputStream(fileName);

		try {
			do {
				// Para testes!
				// System.out.println("O tamanho do pacote # " +
				// tftpReply.getBlockCount() + " e: " + reply.getLength() +
				// " bytes!");
				// Para testes!

				// Acusa a recepÃ§Ã£o de um bloco
				System.out.print(".");
				// verificar se o pacote nao e' duplicado
				if (packetsReceived == 0
						|| tftpReply.getBlockCount() == packetsReceived + 1) {

					// caso o pacote NAO seja vazio
					if (reply.getLength() - DATABYTES != 0) {
						fileBuffer = reply.getData();
						fout.write(fileBuffer, DATABYTES, reply.getLength()
								- DATABYTES);
						totalBytes += reply.getLength() - DATABYTES;
					}
					// Mandar para o servidor e porto de resposta
					sendAck(tftpReply.getBlockCount(), serverAddress, port);
					totalACKs++;

					// medir o tempo entre o envio um ACK N e a recepcao de um
					// packet N+1
					if (packetsReceived > 0) {
						endACKTime = System.currentTimeMillis();
						countACKnPacket++;
						sumACKnPacketTime += (endACKTime - startACKTime);
					}
					startACKTime = System.currentTimeMillis();
					// se for duplicado
				} else if (packetsReceived == tftpReply.getBlockCount()) {
					totalDuplicatePackets++;
					sendAck(tftpReply.getBlockCount(), serverAddress, port);
					totalACKs++;
				}

				// Incrementa o contador interno de blocos recebidos
				packetsReceived = tftpReply.getBlockCount();
				totalPacketsReceived++;

				// No caso de ser o ultimo pacote sai do ciclo, se nao continua
				// a receber
				if (reply.getLength() < blockSize)
					isFinished = true;
				else
					socket.receive(reply);

			} while (!isFinished);
			//System.out.println("Saiu!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			fout.close();
			//System.out.println("Closeeeeeeeeeeeeeeeee");
			double totalTime = System.currentTimeMillis() - startTime;
			System.out.println("\nTotal de bytes transferidos: " + totalBytes);
			System.out
					.println("NuÌ�mero total de pacotes recebidos com dados (incluindo repeticÌ§oÌƒes): "
							+ totalPacketsReceived);
			System.out.println("NuÌ�mero total de pacotes enviados com ACKs: "
					+ totalACKs);
			System.out.println("Tempo total que durou a transfereÌ‚ncia: "
					+ totalTime / 1000 + " segs");
			System.out
					.println("NuÌ�mero total de pacotes de dados recebidos que eram duplicados: "
							+ totalDuplicatePackets);
			if(packetsReceived >1){
				double avgACKnPacketTime = sumACKnPacketTime/countACKnPacket;
				System.out.println("Tempo meÌ�dio que medeia entre o envio de um ACK e a recepcÌ§aÌƒo do pacote de dados seguinte: "+ avgACKnPacketTime + " ms");
				
			}
			
		} catch (Exception e) {
			System.err.printf("Can't copy file\n");
			System.exit(0);
		}

	}

	private static void sendAck(int blockNum, InetAddress serverAddress,
			int port) throws IOException {
		DatagramPacket ack = new DatagramPacket(new byte[4], 4, serverAddress,
				port);
		TftpPacket acktftp = new TftpPacket(ack.getData(), ack.getLength());
		acktftp.setOpcode(TftpPacket.ACK);
		acktftp.setBlockCount(blockNum);
		socket.send(ack);
	}

	private static void sendRequest(String fileName, InetAddress serverAddress,
			int port, int blockSize) {

		DatagramPacket fileNameRequest = new DatagramPacket(
				new byte[BLOCKSIZE + 4], BLOCKSIZE + 4, serverAddress, port);
		TftpPacket fileRequest = new TftpPacket(fileNameRequest.getData(),
				fileNameRequest.getLength());
		fileRequest.setOpcode(TftpPacket.RRQ);
		if(blockSize != BLOCKSIZE){
			fileRequest.setFileName(fileName, MODE, Integer.toString(blockSize));
		}else
			fileRequest.setFilename(fileName, MODE);
													
		try {
			socket.send(fileNameRequest);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro no socket send");
			System.exit(0);
		}

	}

}